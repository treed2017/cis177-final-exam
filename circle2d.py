__author__ = 'treed2017'


class Circle2d:
    def __init__(self, x, y, rad):
        self.y = None
        self.__x = x
        self.__y = y
        self.__rad = rad
        self.__area = 0
        self.__perm = 0

    def setX(self, x):
        self.__x = x

    def getX(self):
        return self.__x

    def setY(self, y):
        self.__y = y

    def getY(self):
        return self.__y

    def setRadius(self, r):
        self.__rad = r

    def getRadius(self):
        return self.__rad

    def getArea(self):
        self.__area = self.x * self.y
        return self.__area

    def getPerimeter(self):
        self.__perm = 2 * 3.14 * self.__rad
        return self.__perm

    def containsPoint(self, x, y):
        return "not working"

    def contains(self, circle2d):
        return "not working"

    def overlaps(self, circle2d):
        return "not working"

